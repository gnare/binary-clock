INCLUDE := -I ~/include
LDFLAGS := -L ~/lib -lsense
SOURCES := main.c display.c
OBJECTS := main.o display.o
TARGET := clock

display.o: display.c
	@echo "Running goal \"display.o\": compiling sources..."
	gcc -c -Wall -Werror display.c $(INCLUDE)

main.o: main.c
	@echo "Running goal \"main.o\": compiling sources..."
	gcc -c -Wall -Werror main.c $(INCLUDE)


all: $(OBJECTS)
	@echo "Running goal \"all\": linking binaries..."
	gcc -o $(TARGET) $(OBJECTS) $(LDFLAGS)
	chmod +x ./$(TARGET)


clean:
	@echo "Running goal \"clean\": removing binaries..."
	-rm $(OBJECTS)
	-rm $(TARGET)


run: all
	./clock.sh | ./$(TARGET)
